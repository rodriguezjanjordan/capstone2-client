let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")
let navbar = document.querySelector("#navbar")



if (token === null){
	navbar.innerHTML=
	`
		<li class="nav-item active">
			<a href="./index.html" class="nav-link"> Home </a>
		</li>
		<li class="nav-item">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
		<li class="nav-item">
			<a href="./pages/login.html" class="nav-link"> Login </a>
		</li>
	`
} else {
	if (adminUser == "false" || !adminUser){
		navbar.innerHTML =
		`	
			<li class="nav-item active">
				<a href="./index.html" class="nav-link"> Home </a>
			</li>
			<li class="nav-item">
				<a href="./pages/courses.html" class="nav-link"> Courses </a>
			</li>
			<li class="nav-item">
				<a href="./pages/profile.html" class="nav-link"> Profile </a>
			</li>
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`
	} else {
		navbar.innerHTML =
		`	
			<li class="nav-item active">
				<a href="./index.html" class="nav-link"> Home </a>
			</li>
			<li class="nav-item">
				<a href="./pages/courses.html" class="nav-link"> Courses </a>
			</li>
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Logout </a>
			</li>
		`
	}
}

