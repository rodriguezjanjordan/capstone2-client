let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")

let navbar = document.querySelector("#navbar")
let modalButton = document.querySelector("#adminButton")
let cardFooter;



if (token === null){
	navbar.innerHTML=
	`
		<li class="nav-item">
			<a href="../index.html" class="nav-link"> Home </a>
		</li>
		<li class="nav-item active">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
		<li class="nav-item">
			<a href="./login.html" class="nav-link"> Login </a>
		</li>
	`
} else {
	if (adminUser == "false" || !adminUser){
		navbar.innerHTML =
		`	
			<li class="nav-item">
				<a href="../index.html" class="nav-link"> Home </a>
			</li>
			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>
			<li class="nav-item">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`
	} else {
		navbar.innerHTML =
		`	<li class="nav-item">
				<a href="../index.html" class="nav-link"> Home </a>
			</li>
			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`
	}
}

if (adminUser == "false" || !adminUser) {
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>
		`
}

if (adminUser == "false" || !adminUser) {
	fetch('https://enigmatic-caverns-73421.herokuapp.com/api/courses/active')
	.then(res => res.json())
	.then(data => {
		let courseData;

		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block">Go to Course</a>`
				
				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
					`
				)
			}).join("")
		}

		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
	})
} else {
	fetch('https://enigmatic-caverns-73421.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		let courseData;

		if(data.length < 1){
			courseData = "No courses available"
		} else {
			courseData = data.map(course => {
				let viewCourse;
				if(`${course.isActive}` == "false"){
					cardFooter = `<a href="./enableCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-success text-white btn-block"> Enable Course </a>`

					viewCourse = `<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-secondary text-white btn-block"> View Course </a>`
				} else {
					cardFooter = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block"> Archive Course </a>`

					viewCourse = `<a href="./course.html?courseId=${course._id}" value=${course._id} class = "btn btn-secondary text-white btn-block"> View Course </a>`
				}
				
				return (
					`
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
								${viewCourse}
							</div>
						</div>
					</div>
					`
				)
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
	})
}
