let params = new URLSearchParams(window.location.search)

let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")

let userFirstName = document.querySelector("#userName")
let userLastName = document.querySelector("#userLastName")
let userEmail = document.querySelector("#userEmail")
let userMobileNumber = document.querySelector("#userMobileNumber")
let enrollmentContainer = document.querySelector("#enrollmentContainer")
let courseName = document.querySelector("#courseName")
let courseDescription = document.querySelector("#courseDescription")

let cardFooter;



fetch("https://enigmatic-caverns-73421.herokuapp.com/api/users/details", {
	method : "GET",
	headers : {
		"Content-Type" : "application/json",
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {
	return res.json()
})
.then(data => {
	userName.innerHTML = data.firstname + " " + data.lastname
	userEmail.innerHTML = data.email
	userMobileNumber.innerHTML = data.mobileNo

	let courseName = data.enrollments
	courseName.map(course => {
		fetch(`https://enigmatic-caverns-73421.herokuapp.com/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {
			courseData = data
			courseData =
				(
				`<div class="col-md-6 my-3 mx-auto">
					<div class="card">
						<div class="card-body text-center">
							<h5 class="card-title">${courseData.name}</h5>
						</div>
					</div>
				</div>`
				)
			container.innerHTML += courseData
		})
		let container = document.querySelector("#coursesContainer")
	})
})

